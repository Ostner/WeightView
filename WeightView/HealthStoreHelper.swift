//
//  HealthStoreHelper.swift
//  WeightView
//
//  Created by Tobias Ostner on 1/16/17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import Foundation
import HealthKit


struct HealthStoreHelper {

    let healthStore: HKHealthStore

    init?() {
        guard HKHealthStore.isHealthDataAvailable() else { return nil }
        healthStore = HKHealthStore()
    }

    func authorize(completion: @escaping (Bool, Error?) -> Void) {
        let toRead: Set<HKObjectType> =
          [HKObjectType.quantityType(forIdentifier: .bodyMass)!,
           HKObjectType.quantityType(forIdentifier: .bodyFatPercentage)!,
           HKObjectType.quantityType(forIdentifier: .bodyMassIndex)!]
        healthStore.requestAuthorization(toShare: nil, read: toRead) {
            success, error in
            completion(success, error)
        }
    }

    func query<T>(for resource: Resource<HKStatisticsCollection, T>, completion: @escaping (T?) -> ()) {
        let query = HKStatisticsCollectionQuery(
          quantityType: resource.quantity,
          quantitySamplePredicate: nil,
          options: .discreteAverage,
          anchorDate: resource.anchor,
          intervalComponents: resource.interval)
        query.initialResultsHandler = {
            query, result, error in
            completion(resource.convert(result))
        }
        healthStore.execute(query)
    }

    func queryStats(for resource: Resource<HKStatistics, [Double?]>, completion: @escaping ([Double?]?) -> ()) {
        let query = HKStatisticsQuery(
          quantityType: resource.quantity,
          quantitySamplePredicate: HKQuery.predicateForSamples(withStart: resource.duration.startDate, end: .now, options: []),
          options: [.discreteAverage, .discreteMax, .discreteMin]) {
            query, result, error in
            completion(resource.convert(result))
        }
        healthStore.execute(query)
    }

}
