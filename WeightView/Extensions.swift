//
//  Extensions.swift
//  WeightView
//
//  Created by Tobias Ostner on 1/25/17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import UIKit
import HealthKit

extension Date {

    static var now: Date {
        return Date()
    }

    static var weekAgo: Date {
        return Calendar.current.date(byAdding: .day, value: -7, to: .now)!
    }

    static var monthAgo: Date {
        return Calendar.current.date(byAdding: .month, value: -1, to: .now)!
    }

    static var yearAgo: Date {
        return Calendar.current.date(byAdding: .year, value: -1, to: .now)!
    }

    static var startOfDay: Date {
        return Calendar.current.startOfDay(for: .now)
    }

    func dayDistance(to other: Date) -> Int {
        let components = Calendar.current.dateComponents([.day], from: self, to: other)
        return components.day!
    }
}


enum UnitIdentifier: String {
    case kg = "kg"
    case percent = "%"
}

extension HKUnit {
    convenience init(from identifier: UnitIdentifier) {
        self.init(from: String(describing: identifier))
    }

    static var kg: HKUnit {
        return HKUnit(from: .kg)
    }

    static var percent: HKUnit {
        return HKUnit(from: "%")
    }

    static var count: HKUnit {
        return HKUnit(from: "count")
    }
}
