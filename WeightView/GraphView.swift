//
//  GraphView.swift
//  WeightView
//
//  Created by Tobias Ostner on 1/17/17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import UIKit

class GraphView: UIView {

    var plotData: PlotData? {
        didSet {
            setNeedsDisplay()
        }
    }

    var isLoss: Bool? {
        didSet {
            setNeedsDisplay()
        }
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }

    private func setup() {
        layer.cornerRadius = 5
        clipsToBounds = true
    }

    override func draw(_ rect: CGRect) {
        guard let _ = plotData else { return }
        let context = UIGraphicsGetCurrentContext()!
        drawAxis(in: context)
        drawLabels(in: context)
        drawGraph(in: context)
        drawPointMarker(in: context)
    }

    fileprivate let lossColor = UIColor(
      colorLiteralRed: 16/255,
      green: 99/255,
      blue: 26/255,
      alpha: 1)

    fileprivate let gainColor = UIColor(
      colorLiteralRed: 128/255,
      green: 21/255,
      blue: 21/255,
      alpha: 1)

    fileprivate let gridColor = UIColor(
      white: 0.5,
      alpha: 1.0)

    fileprivate let font = UIFont.systemFont(ofSize: 12)

    fileprivate let lPadding: CGFloat = 40
    fileprivate let tPadding: CGFloat = 20
    fileprivate let rPadding: CGFloat = 5
    fileprivate let bPadding: CGFloat = 30

}

extension GraphView {

    fileprivate func drawAxis(in context: CGContext) {
        context.saveGState()
        gridColor.setStroke()
        context.setLineWidth(2)
        // Draw the x axis
        context.move(to: origin)
        context.addLine(to: CGPoint(x: origin.x + graphWidth, y: origin.y))
        context.strokePath()
        // Draw an upperbound x axis
        context.setLineDash(phase: 0, lengths: [3,2])
        context.move(to: CGPoint(x: origin.x, y: origin.y - graphHeight))
        context.addLine(to: CGPoint(x: origin.x + graphWidth, y: origin.y - graphHeight))
        context.strokePath()
        // Draw an axis in between those two
        context.setLineDash(phase: 0, lengths: [3,2])
        context.move(to: CGPoint(x: origin.x, y: origin.y - graphHeight/2))
        context.addLine(to: CGPoint(x: origin.x + graphWidth, y: origin.y - graphHeight/2))
        context.strokePath()
        context.restoreGState()
    }

    fileprivate func drawLabels(in context: CGContext) {
        guard let plotData = plotData,
              !plotData.points.isEmpty
        else { return }
        context.saveGState()
        // Draw y labels
        let paragraph = NSMutableParagraphStyle()
        paragraph.alignment = .center
        let attributes = [NSForegroundColorAttributeName: gridColor,
                          NSFontAttributeName: font,
                          NSParagraphStyleAttributeName: paragraph]
        let upperLbl = plotData.yLabel(for: Float(upperBound))
        let estimate = upperLbl.boundingRect(
          with: CGSize(width: lPadding, height: 1000),
          attributes: attributes,
          context: nil
        )
        upperLbl.draw(
          in: CGRect(x: 0,
                     y: origin.y - graphHeight - estimate.height,
                     width: lPadding,
                     height: estimate.height),
          withAttributes: attributes)
        let originLbl = plotData.yLabel(for: Float(lowerBound))
        originLbl.draw(
          in: CGRect(x: 0,
                     y: origin.y - estimate.height,
                     width: lPadding,
                     height: estimate.height),
          withAttributes: attributes)
        let inBetweenValue = (upperBound - lowerBound) / 2 + lowerBound
        let inBetweenLbl = plotData.yLabel(for: Float(inBetweenValue))
        inBetweenLbl.draw(
          in: CGRect(x: 0,
                     y: origin.y - graphHeight/2 - estimate.height,
                     width: lPadding,
                     height: estimate.height),
          withAttributes: attributes)
        // Draw x labels
        let xPositions = plotData.xLabels.indices.map {
            CGFloat($0) * self.graphWidth / CGFloat(plotData.xLabels.count) + origin.x
        }
        let y = origin.y
        for (index, label) in plotData.xLabels.enumerated() {
            if let label = label {
                let string = label as NSString
                string.draw(
                  at: CGPoint(x: xPositions[index], y: y),
                  withAttributes: [NSForegroundColorAttributeName: gridColor])
            }
        }
        context.restoreGState()
    }

    fileprivate func drawGraph(in context: CGContext) {
        guard let points = plotData?.points,
              !points.isEmpty
        else { return }
        context.saveGState()
        let scaled = points.map(scalePoint)
        // Draw the line graph
        context.setLineWidth(2)
        context.setStrokeColor(color.cgColor)
        context.move(to: scaled.first!)
        for point in scaled.suffix(from: 1) {
            context.addLine(to: point)
        }
        context.strokePath()
        // Fill the graph
        context.move(to: scaled.first!)
        for point in scaled.suffix(from: 1) {
            context.addLine(to: point)
        }
        context.addLine(to: CGPoint(x: scaled.last!.x, y: origin.y))
        context.addLine(to: CGPoint(x: scaled.first!.x, y: origin.y))
        context.closePath()
        color.withAlphaComponent(0.2).setFill()
        context.fillPath()
        context.restoreGState()
    }

    fileprivate func drawPointMarker(in context: CGContext) {
        guard let points = plotData?.points,
              !points.isEmpty
        else { return }
        context.saveGState()
        context.setLineWidth(2)
        context.setFillColor(UIColor.white.cgColor)
        context.setStrokeColor(color.cgColor)
        let scaled = points.map(scalePoint)
        for point in scaled {
            context.addEllipse(in: CGRect(x: point.x - 4,
                                          y: point.y - 4,
                                          width: 8,
                                          height: 8))
        }
        context.drawPath(using: .fillStroke)
        context.restoreGState()
    }

    fileprivate var scalePoint: (CGPoint) -> CGPoint {
        return {
            point in
            let x = point.x * self.xPointFactor + self.lPadding
            let y = self.origin.y - self.yScale * (point.y - self.lowerBound)
            return CGPoint(x: x, y: y)
        }
    }

    fileprivate var color: UIColor {
        guard let isLoss = isLoss else { return UIColor(white: 0.7, alpha: 1) }
        return isLoss ? lossColor : gainColor
    }

    fileprivate var graphWidth: CGFloat {
        return self.bounds.width - lPadding - rPadding
    }

    fileprivate var graphHeight: CGFloat {
        return origin.y - tPadding
    }

    fileprivate var xPointFactor: CGFloat {
        guard let plotData = plotData else { return 1 }
        return graphWidth / CGFloat(plotData.samples.count)
    }

    fileprivate var yScale: CGFloat {
        return graphHeight / (upperBound - lowerBound)
    }

    fileprivate var maxX: CGFloat? {
        let maxXPoint = plotData?.points.max() {
            p1, p2 in
            return p1.x < p2.x
        }
        return maxXPoint?.x
    }

    fileprivate var maxY: CGFloat? {
        let maxYPoint = plotData?.points.max() {
            p1, p2 in
            return p1.y < p2.y
        }
        return maxYPoint?.y
    }

    fileprivate var minY: CGFloat? {
        let minYPoint = plotData?.points.min() {
            p1, p2 in
            return p1.y < p2.y
        }
        return minYPoint?.y
    }

    // Lower bound of graph in data units
    fileprivate var lowerBound: CGFloat {
        guard let min = minY, let max = maxY else { return 0 }
        return min - (max - min)*0.2
    }

    // Upper bound of graph in data units
    fileprivate var upperBound: CGFloat {
        guard let min = minY, let max = maxY else { return 0 }
        return max + (max - min)*0.2
    }

    fileprivate var origin: CGPoint {
        return CGPoint(x: lPadding, y: self.bounds.height-bPadding)
    }

}
