//
//  Duration.swift
//  WeightView
//
//  Created by Tobias Ostner on 2/22/17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import Foundation

enum Duration: Int {
    case week
    case month
    case year

    var startDate: Date {
        switch self {
        case .week:
            return Date.weekAgo
        case .month:
            return Date.monthAgo
        case .year:
            return Date.yearAgo
        }
    }
}
