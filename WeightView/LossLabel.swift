//
//  LossLabel.swift
//  WeightView
//
//  Created by Tobias Ostner on 1/31/17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import UIKit

class LossLabel: UILabel {

    @IBInspectable var unit: String = ""

    let gainColor = UIColor.red
    let lossColor = UIColor(colorLiteralRed: 82/255, green: 165/255, blue: 92/255, alpha: 1)

    var value: Double = 0.0 {
        didSet {
            textColor = value > 0 ? gainColor : lossColor
            let sign = value > 0 ? "+" : ""
            self.text = String(format: "\(sign)%.02f \(unit)", value)
        }
    }

}
