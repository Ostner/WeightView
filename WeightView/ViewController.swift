//
//  ViewController.swift
//  WeightView
//
//  Created by Tobias Ostner on 1/15/17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import UIKit
import HealthKit

class ViewController: UIViewController {

    fileprivate var healthStore: HealthStoreHelper!

    @IBOutlet weak var segment: UISegmentedControl!

    @IBOutlet weak var weightGraph: GraphView!
    @IBOutlet weak var weightLossLbl: LossLabel!
    @IBOutlet weak var weightDetails: UIView!
    @IBOutlet weak var weightAverageLbl: UILabel!
    @IBOutlet weak var weightMaxLbl: UILabel!
    @IBOutlet weak var weightMinLbl: UILabel!

    @IBOutlet weak var bodyFatGraph: GraphView!
    @IBOutlet weak var bodyFatLbl: LossLabel!
    @IBOutlet weak var bodyFatStats: UIView!
    @IBOutlet weak var bodyFatAverageLbl: UILabel!
    @IBOutlet weak var bodyFatMaxLbl: UILabel!
    @IBOutlet weak var bodyFatMinLbl: UILabel!

    @IBOutlet weak var bmiGraph: GraphView!
    @IBOutlet weak var bmiLbl: LossLabel!
    @IBOutlet weak var bmiStats: UIView!
    @IBOutlet weak var bmiAverageLbl: UILabel!
    @IBOutlet weak var bmiMaxLbl: UILabel!
    @IBOutlet weak var bmiMinLbl: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        guard let helper = HealthStoreHelper() else { alert(message: "Not data available"); return }
        healthStore = helper
        healthStore.authorize() {
            success, error in
            if !success {
                DispatchQueue.main.async {
                    self.alert(message: "Authorization error: \(error?.localizedDescription)")
                }
                return
            }
            self.queryData(duration: Duration(rawValue: self.segment.selectedSegmentIndex) ?? .week)
        }

    }

    @IBAction func tabWeight(sender: UITapGestureRecognizer) {
        guard let view = sender.view else { return }
        switch view {
        case weightGraph:
            weightDetails.isHidden = !weightDetails.isHidden
        case bodyFatGraph:
            bodyFatStats.isHidden = !bodyFatStats.isHidden
        case bmiGraph:
            bmiStats.isHidden = !bmiStats.isHidden
        default:
            return
        }
    }
}

extension ViewController {

    fileprivate func alert(message: String) {
        let alert = UIAlertController(
          title: "HealthStore Failure",
          message: message,
          preferredStyle: .alert)
        let ok = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        alert.addAction(ok)
        present(alert, animated: true, completion: nil)
    }

    fileprivate func createSampleResource(
      for type: HKQuantityTypeIdentifier,
      duration: Duration,
      unit: HKUnit) -> Resource<HKStatisticsCollection, PlotData>
    {
        return Resource(
          typeIdentifier: type,
          duration: duration,
          dayInterval: duration == .week ? 1 : duration == .month ? 3 : 30,
          anchor: .startOfDay) {
            statistics in
            return PlotData(statistics: statistics, duration: duration, unit: unit)
        }
    }

    fileprivate func createSummaryResource(
      for type: HKQuantityTypeIdentifier,
      duration: Duration,
      unit: HKUnit) -> Resource<HKStatisticsCollection, Double>
    {
        return Resource(
          typeIdentifier: type,
          duration: duration,
          dayInterval: 1,
          anchor: .startOfDay) {
            statistics in
            guard let statistics = statistics else { return nil }
            var allSamples: [Double?] = []
            statistics.enumerateStatistics(from: duration.startDate, to: .now) {
                statistic, _ in
                allSamples.append((statistic.averageQuantity()?.doubleValue(for: unit)))
            }
            let samples = allSamples.flatMap { $0 }
            if samples.isEmpty {
                return 0
            } else {
                return samples.last! - samples.first!
            }
        }
    }

    fileprivate func createStatsResource(
      for type: HKQuantityTypeIdentifier,
      duration: Duration,
      unit: HKUnit) -> Resource<HKStatistics, [Double?]>
    {
        return Resource(
          typeIdentifier: type,
          duration: duration,
          dayInterval: 1,
          anchor: .now) {
            statistics in
            guard let statistics = statistics else { return nil }
            return [statistics.averageQuantity()?.doubleValue(for: unit),
                    statistics.maximumQuantity()?.doubleValue(for: unit),
                    statistics.minimumQuantity()?.doubleValue(for: unit)]

        }
    }

    fileprivate func updateWeight(resource: Resource<HKStatisticsCollection, PlotData>) {
        healthStore.query(for: resource) {
            plotData in
            guard let plotData = plotData else { print("No quantities"); return }
            DispatchQueue.main.async {
                self.weightGraph.plotData = plotData
            }
        }
    }

    fileprivate func updateWeigthSummary(resource: Resource<HKStatisticsCollection, Double>) {
        healthStore.query(for: resource) {
            value in
            guard let value = value else { return }
            DispatchQueue.main.async {
                self.weightLossLbl.value = value
                self.weightGraph.isLoss = value > 0 ? false : true
            }
        }
    }

    fileprivate func updateWeightStats(resource: Resource<HKStatistics, [Double?]>) {
        healthStore.queryStats(for: resource) {
            values in
            guard let values = values else { return }
            DispatchQueue.main.async {
                self.weightAverageLbl.text = String(format: "Average: %.2f kg", values[0] ?? 0.0)
                self.weightMaxLbl.text = String(format: "Maximum: %.2f kg", values[1] ?? 0.0)
                self.weightMinLbl.text = String(format: "Minimum: %.2f kg", values[2] ?? 0.0)
            }
        }
    }

    fileprivate func updateBodyFatStats(resource: Resource<HKStatistics, [Double?]>) {
        healthStore.queryStats(for: resource) {
            values in
            guard let values = values else { return }
            DispatchQueue.main.async {
                guard let avg = values[0], let max = values[1], let min = values[2] else { return }
                self.bodyFatAverageLbl.text = String(format: "Average: %.2f %%", avg*100)
                self.bodyFatMaxLbl.text = String(format: "Maximum: %.2f %%", max*100)
                self.bodyFatMinLbl.text = String(format: "Minimum: %.2f %%", min*100)
            }
        }
    }

    fileprivate func updateBMIStats(resource: Resource<HKStatistics, [Double?]>) {
        healthStore.queryStats(for: resource) {
            values in
            guard let values = values else { return }
            DispatchQueue.main.async {
                self.bmiAverageLbl.text = String(format: "Average: %.2f", values[0] ?? 0.0)
                self.bmiMaxLbl.text = String(format: "Maximum: %.2f", values[1] ?? 0.0)
                self.bmiMinLbl.text = String(format: "Minimum: %.2f", values[2] ?? 0.0)
            }
        }
    }

    fileprivate func updateBodyFat(resource: Resource<HKStatisticsCollection, PlotData>) {
        healthStore.query(for: resource) {
            plotData in
            guard let plotData = plotData else { print("No quantities"); return }
            DispatchQueue.main.async {
                self.bodyFatGraph.plotData = plotData
            }
        }
    }

    fileprivate func updateBodyFatSummary(resource: Resource<HKStatisticsCollection, Double>) {
        healthStore.query(for: resource) {
            value in
            guard let value = value else { return }
            DispatchQueue.main.async {
                self.bodyFatLbl.value = value
                self.bodyFatGraph.isLoss = value > 0 ? false : true
            }
        }
    }

    fileprivate func updateBMI(resource: Resource<HKStatisticsCollection, PlotData>) {
        healthStore.query(for: resource) {
            plotData in
            guard let plotData = plotData else { print("No quantities"); return }
            DispatchQueue.main.async {
                self.bmiGraph.plotData = plotData
            }
        }
    }

    fileprivate func updateBMISummary(resource: Resource<HKStatisticsCollection, Double>) {
        healthStore.query(for: resource) {
            value in
            guard let value = value else { return }
            DispatchQueue.main.async {
                self.bmiLbl.value = value
                self.bmiGraph.isLoss = value > 0 ? false : true
            }
        }
    }

    fileprivate func queryData(duration: Duration) {
        updateWeight(resource: createSampleResource(
                       for: .bodyMass,
                       duration: duration,
                       unit: .kg))
        updateWeigthSummary(resource: createSummaryResource(
                       for: .bodyMass,
                       duration: duration,
                       unit: .kg))
        updateBodyFat(resource: createSampleResource(
                        for: .bodyFatPercentage,
                        duration: duration,
                        unit: .percent))
        updateBodyFatSummary(resource: createSummaryResource(
                               for: .bodyFatPercentage,
                               duration: duration,
                               unit: .percent))
        updateBMI(resource: createSampleResource(
                    for: .bodyMassIndex,
                    duration: duration,
                    unit: .count))
        updateBMISummary(resource: createSummaryResource(
                           for: .bodyMassIndex,
                           duration: duration,
                           unit: .count))
        updateWeightStats(resource: createStatsResource(
                            for: .bodyMass,
                            duration: duration,
                            unit: .kg))
        updateBodyFatStats(resource: createStatsResource(
                             for: .bodyFatPercentage,
                             duration: duration,
                             unit: .percent))
        updateBMIStats(resource: createStatsResource(
                         for: .bodyMassIndex,
                         duration: duration,
                         unit: .count))
    }

}

extension ViewController {

    @IBAction func didChangeInterval(_ sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex {
        case 0:
            queryData(duration: .week)
        case 1:
            queryData(duration: .month)
        case 2:
            queryData(duration: .year)
        default:
            fatalError("Illegal segment index")
        }
    }
}
