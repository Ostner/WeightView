//
//  Resource.swift
//  WeightView
//
//  Created by Tobias Ostner on 1/19/17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import Foundation
import HealthKit

struct Resource<A, B> {
    let typeIdentifier: HKQuantityTypeIdentifier
    let duration: Duration
    let dayInterval: Int
    let anchor: Date
    let convert: (A?) -> B?

    var quantity: HKQuantityType {
        return HKObjectType.quantityType(forIdentifier: typeIdentifier)!
    }

    var interval: DateComponents {
        var component = DateComponents()
        component.day = dayInterval
        return component
    }

}
