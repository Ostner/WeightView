//
//  PlotData.swift
//  WeightView
//
//  Created by Tobias Ostner on 1/31/17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import UIKit
import HealthKit

struct PlotData {

    let samples: [(Date, Double?)]

    let duration: Duration

    let unit: HKUnit

    init?(statistics: HKStatisticsCollection?, duration: Duration, unit: HKUnit) {
        guard let statistics = statistics else { return nil }
        var samples: [(Date, Double?)] = []
        statistics.enumerateStatistics(from: duration.startDate, to: .now) {
            statistic, _ in
            let value = statistic.averageQuantity()?.doubleValue(for: unit)
            samples.append((statistic.endDate, value))
        }
        self.samples = samples
        self.duration = duration
        self.unit = unit
    }

    var points: [CGPoint] {
        var idx = 0
        let points = self.samples.flatMap {
            (_, quantity) -> CGPoint? in
            defer { idx += 1 }
            if let quantity = quantity {
                return CGPoint(x: CGFloat(idx), y: CGFloat(quantity))
            }
            return nil
        }
        return points
    }

    var xLabels: [String?] {
        switch duration {
        case .week:
            return weekdays
        case .month:
            return monthDays
        case .year:
            return months
        }
    }

    func yLabel(for value: Float) -> String {
        switch unit.unitString {
        case "kg", "count":
            return String(format: "%.1f", value)
        case "%":
            return String(format: "%.1f", value * 100)
        default:
            return "no format"
        }
    }

    fileprivate var weekdays: [String?] {
        return samples.map { sample in
            let component = Calendar.current.dateComponents(
              [.weekday],
              from: sample.0)
            return Calendar.current.shortWeekdaySymbols[component.weekday! - 1]
        }
    }

    fileprivate var monthDays: [String?] {
        var result: [String?] = []
        let formatter = DateFormatter()
        formatter.dateFormat = "d MMM"
        for idx in samples.indices {
            if idx % 3 == 0 {
                result.append(formatter.string(from: samples[idx].0))
            } else {
                result.append(nil)
            }
        }
        return result
    }

    fileprivate var months: [String?] {
        let formatter = DateFormatter()
        formatter.dateFormat = "d MMM"
        return samples.indices.map {
            index in
            if index % 3 == 0 {
                return formatter.string(from: samples[index].0)
            } else {
                return nil
            }
        }
    }

}
